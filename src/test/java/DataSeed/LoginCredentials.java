package DataSeed;


public enum LoginCredentials {
    WRONGUSER("hcpgben2", "Rscon112");

    private String login;
    private String password;

    LoginCredentials(String login, String password) {
        this.login = login;
        this.password = password;
    }

    public String getLogin() {
        return this.login;
    }

    public String getPassword() {
        return this.password;
    }
}