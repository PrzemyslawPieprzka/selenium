package SetUp;

import DataSeed.PageUrls;
import org.junit.*;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import java.util.concurrent.TimeUnit;

public class WebDriverTestBase {
    public WebDriver driver;
    public final String START_LOGIN_PAGE = PageUrls.MSdialog + "login";

    @Before
    public void setUp() {

        driver = new FirefoxDriver();
        driver.manage()
              .timeouts()
              .implicitlyWait(10, TimeUnit.SECONDS);
    }

    @After
    public void endTest() {
        driver.close();
    }
}