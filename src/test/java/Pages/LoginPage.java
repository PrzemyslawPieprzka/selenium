package Pages;

import DataSeed.LoginCredentials;
import DataSeed.Messages;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;


public class LoginPage extends PageCommons {

    @FindBy(id = "username")
    public WebElement loginField;

    @FindBy(id = "password")
    public WebElement passwordField;

    @FindBy(css = "[type=submit]")
    public WebElement submitButton;

    @FindBy(xpath = "//*[@class='loginErrors']//*[contains(text(),'" + Messages.ERROR_MESSAGE + "')]")
    public WebElement errorMessage;

    private String url;

    public LoginPage(String url, WebDriver driver) throws InterruptedException {
        super(driver);
        this.url = url;
        PageFactory.initElements(driver, this);
    }

    public LoginPage open() {
        driver.navigate().to(url);
        return this;
    }

    public SignedInPage loginAs(LoginCredentials loginCredentials) {

        printTextToField(loginCredentials.getLogin(), loginField);
        printTextToField(loginCredentials.getPassword(), passwordField);

        submitButton.click();
        return new SignedInPage(driver);
    }
}