package Pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class PageCommons {

    public WebDriver driver;

    public PageCommons(WebDriver driver) {
        this.driver = driver;
    }

    public void printTextToField(String textToWrite, WebElement textBox) {
        textBox.clear();
        textBox.sendKeys(textToWrite);
    }
}