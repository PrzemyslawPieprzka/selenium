package Tests;

import DataSeed.LoginCredentials;
import Pages.LoginPage;
import SetUp.WebDriverTestBase;
import org.junit.Test;

import static org.junit.Assert.assertTrue;

public class TestLogin extends WebDriverTestBase {

    @Test
    public void testWrongUserCannotLogin() throws InterruptedException {

        LoginPage loginPage = new LoginPage(START_LOGIN_PAGE, driver);
        loginPage.open()
                 .loginAs(LoginCredentials.WRONGUSER);

        assertTrue(loginPage.errorMessage
                .isDisplayed());
    }
}